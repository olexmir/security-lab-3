package com.oleksmir.security.utils;

public class Storage {

    private byte[] newHMAC;

    private byte[] newMessage;

    public Storage(byte[] newHMAC, byte[] newMessage) {
        this.newHMAC = newHMAC;
        this.newMessage = newMessage;
    }

    public byte[] getNewHMAC() {
        return newHMAC;
    }

    public void setNewHMAC(byte[] newHMAC) {
        this.newHMAC = newHMAC;
    }

    public byte[] getNewMessage() {
        return newMessage;
    }

    public void setNewMessage(byte[] newMessage) {
        this.newMessage = newMessage;
    }
}
