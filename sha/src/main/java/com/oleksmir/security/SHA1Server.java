package com.oleksmir.security;

import com.oleksmir.security.utils.SHA1;
import com.oleksmir.security.utils.Utils;

public class SHA1Server {

    public byte[] getHash(String secret, String message) {
        SHA1 sha1 = new SHA1();
        return sha1.digest((secret + message).getBytes());
    }

    public String getHashString(String secret, String message) {
        return Utils.toHexString(getHash(secret, message));
    }

}
