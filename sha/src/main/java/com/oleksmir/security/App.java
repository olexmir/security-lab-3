package com.oleksmir.security;

import com.oleksmir.security.utils.Storage;
import com.oleksmir.security.utils.Utils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

    private static final String HOST = "http://ec2-35-159-11-170.eu-central-1.compute.amazonaws.com";

    private static final String PATH = "/mac/trythis";

    private static final String HASH =
            "D62844972D2E3ACD79C1AD479853964183B86147";

    private static final String MESSAGE =
            "Guys who understand that using Hash function as Mac is " +
                    "one very bad practice: Thai Duong; Juliano Rizzo; " +
                    "Flickr (the hard way);";


    public static void main(String[] args) throws IOException {

        String secret = "secret";
        String data = "data";
        String append = "append";

        checkHash(secret, data, append);
        System.out.println("Result 1");
        checkHash("secret", "data", "append");
        System.out.println("Result 2");
        checkHash("dahskjdaskldsakldjlkasd", MESSAGE, "Oleksii Miroshnyk");

//        checkServerHash();

        System.out.println();
        System.out.println();
    }

    private static boolean checkServerHash() throws IOException {

        SHA1Attacker sha1Attacker = new SHA1Attacker();

        for (int i = 1; i < 64; i++) {
            Storage result = sha1Attacker.getStorage(i, HASH, MESSAGE, "Appenddata");

            byte[] newHMAC = result.getNewHMAC();
            byte[] newMessage = result.getNewMessage();

            if (send(newHMAC, newMessage)) {
                System.out.println("YOU HACKED THIS SHA!");
                return true;
            }

        }

        return false;
    }

    private static boolean checkHash(String secret, String data, String append) throws IOException {
        SHA1Attacker sha1Attacker = new SHA1Attacker();
        SHA1Server sha1Server = new SHA1Server();

        String hash = sha1Server.getHashString(secret, data);
        System.out.println("INIT SHA: " + hash);

        for (int i = 1; i < 64; i++) {

            Storage result = sha1Attacker.getStorage(i, hash, data, append);

            byte[] newHMAC = result.getNewHMAC();
            byte[] newMessage = result.getNewMessage();


            String serverNewHMAC = sha1Attacker.getServerNewHMAC(secret.getBytes(), newMessage);
            String hexNewHMAC = Utils.toHexString(newHMAC);

            System.out.println("SECRET LENGHT: " + i);
            System.out.println("NEW HASH: " + hexNewHMAC);
            System.out.println("FINAL: " + serverNewHMAC);
            System.out.println("IS EQUALS: " + serverNewHMAC.equals(hexNewHMAC));

            if (serverNewHMAC.equals(hexNewHMAC)) {
                return true;
            }

            System.out.println();
        }
        return false;
    }

    private static boolean send(byte[] hmac, byte[] message) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();

        HttpPost httpPost = new HttpPost(HOST + PATH);

        MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
//        entityBuilder.addTextBody("message", new String(message, "ASCII"));
//        entityBuilder.addTextBody("mac", new String(hmac, "ASCII"));
        entityBuilder.addBinaryBody("message", message);
        entityBuilder.addBinaryBody("mac", hmac);

        HttpEntity httpEntity = entityBuilder.build();

        httpPost.setEntity(httpEntity);

        CloseableHttpResponse response = httpClient.execute(httpPost);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();

        String line = "";
        while ((line = bufferedReader.readLine()) != null) {
            result.append(line);
        }

        JSONObject json = new JSONObject(result.toString());

        System.out.println(json.getString("message") + "\n");

        return json.getBoolean("correct");
    }
}
